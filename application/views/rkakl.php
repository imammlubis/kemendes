<title>
	Kemendesa | RKAKL
</title>
<div class="m-grid__item m-grid__item--fluid m-wrapper">		
	<div class="m-subheader ">
	<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					RKAKL
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url();?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								RKAKL
							</span>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>

	<div class="m-content" style="width:100%"> 
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-accent"></i>
		</div>
		<div class="m-alert__text">
			RKAKL Information
		</div>
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						RKAKL DATA
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
		<!-- <form method="post" action="<?php echo base_url(); ?>rkakl/export_excel">
									<input type="submit" name="export" class="btn btn-success" value="Export" />
								</form> -->
		
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
						<form method="post" id="import_form" enctype="multipart/form-data">
							<p><label>Select Excel File</label>
							<input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
							<br />
							<input type="submit" name="import" value="Import" class="btn btn-info" />
						</form>							
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">					
					<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<form method="post" action="<?php echo base_url(); ?>rkakl/export_excel">
									<input type="submit" name="export" class="btn btn-success" value="Export" />
								</form>						
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
					
					<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
			<!--end: Search Form -->
<!--begin: Datatable -->
			<div class="m_datatable m-datatable m-datatable--default m-datatable--error m-datatable--loaded" 
			id="table2" style="overflow-x:auto;">
			<!-- <table class="m-datatable__table" 
				id="table"
				style="display: block; min-height: 300px; overflow-x: auto;"><thead class="m-datatable__head">
				<tr class="m-datatable__row" style="left: 0px;"><th data-field="RecordID" 
				class="m-datatable__cell--center m-datatable__cell m-datatable__cell--sort">
				<span style="width: 40px;" >Action</span></th>
				<th data-field="kode" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 150px;" >kode</span></th>
				<th data-field="uraian" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">uraian</span></th>
				<th data-field="vol" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">vol</span></th>
				<th data-field="sat" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 200px;">sat</span></th>
				<th data-field="hargasat" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">hargasat</span></th>
				<th data-field="jumlah" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">jumlah</span></th>
				<th data-field="sdana" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">sdana</span></th>
				<th data-field="realisasi_bulan_lalu" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_bulan_lalu</span></th>
				<th data-field="realisasi_bulan_ini" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_bulan_ini</span></th>
				<th data-field="realisasi_rp" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_rp</span></th>
				<th data-field="realisasi_percent" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_percent</span></th>
				<th data-field="sisa_anggaran" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">sisa_anggaran</span></th>
				<th data-field="created_date" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">created_date</span></th>				
				</tr></thead>
			</table> -->

			<table class="m-datatable__table" 
				id="table"
				style="display: block; min-height: 300px; overflow-x: auto;">
				<thead class="m-datatable__head">
				<tr class="m-datatable__row" style="left: 0px;"><th data-field="id" rowspan="2"
				class="m-datatable__cell--center m-datatable__cell m-datatable__cell--sort">
				<span style="width: 40px;" >Action</span></th>
				<th data-field="kode" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 150px;" >Kode</span></th>
				<th data-field="uraian" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">Uraian</span></th>
				<th data-field="vol" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">Vol</span></th>
				<th data-field="sat" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 200px;">Sat</span></th>
				<th data-field="hargasat" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">HargaSat</span></th>
				<th data-field="jumlah" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">Jumlah</span></th>
				<th data-field="sdana" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">SDana</span></th>
				<th data-field="realisasi" colspan="4" align="center" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Realisasi</span></th>
				<!-- <th data-field="realisasi_bulan_ini" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_bulan_ini</span></th> -->
				<!-- <th data-field="realisasi_rp" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_rp</span></th>
				<th data-field="realisasi_percent" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">realisasi_percent</span></th> -->
				<th data-field="sisa_anggaran" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Sisa Anggaran</span></th>
				<th data-field="created_date" rowspan="2" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Date</span></th>				
				</tr>
				<tr>
					<th data-field="realisasi_bulan_lalu" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Bulan Lalu</span></th>	
					<th data-field="realisasi_bulan_ini" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Bulan Ini</span></th>	
					<th data-field="realisasi_rp" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Rp</span></th>
					<th data-field="realisasi_percent" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">Percentage</span></th>
				</tr>
			</thead>
			</table>

			</div>
			<!--end: Datatable -->
		</div>
	</div>
</div>

<!--Modal 1-->
        <!-- Bootstrap modal -->
        <div class="modal fade" id="modal_form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
											<h3 class="m-portlet__head-text">
												Edit Data, Realisasi Keuangan
											</h3>
										</div>
									</div>
								</div>
								<!--begin::Form-->
								<form class="m-form m-form--fit m-form--label-align-right" id="form">
									<div class="m-portlet__body">
										<div class="form-group m-form__group">
											<label>
												S/D BULAN LALU (Rp)
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" class="form-control m-input"
												id="realisasi_bulan_lalu"  name="realisasi_bulan_lalu"
												placeholder="S/D BULAN LALU (Rp)" >
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												BULAN INI
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="realisasi_bulan_ini" name="realisasi_bulan_ini"
												class="form-control m-input" placeholder="BULAN INI">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>										
										<input type="hidden" value="" name="id"/>
										<div class="form-group m-form__group">
											<label>
												Created Date
											</label>
											<div class="m-input-icon m-input-icon--left">
											<input type="date" class="form-control datepicker" id="created_date" name="created_date" >
											</div>
										</div>
										<div class="m-form__actions">
											<button type="submit" class="btn btn-success" 
												onclick="save()" id="btnSave">
												Submit
											</button>
										</div>
									</div>
								</form>
								<!--end::Form-->
							</div>
                </div><!-- /.modal-content -->
					</div>
				
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End Bootstrap modal -->
        <!--End Modal1-->

<script type="text/javascript">
	var table;
	
	$(document).ready(function() {
		
			table = $('#table').DataTable({
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url('rkakl/ajax_list')?>",
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columnDefs": [
				{
					"targets": [ 0 ], //first column / numbering column
					"orderable": true //set not orderable
				},
			],
			});

		$('#import_form').on('submit', function(event){
			event.preventDefault();
			$.ajax({
				url:"<?php echo base_url(); ?>rkakl/import",
				method:"POST",
				data:new FormData(this),
				contentType:false,
				cache:false,
				processData:false,
				success:function(data){
					$('#file').val('');
					//load_data();
					table.ajax.reload(); 
					alert(data);
				}
			})
		});
	});

		function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

            url = "<?php echo site_url('rkakl/ajax_update')?>";

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
					$('#modal_form').modal('hide');
                    table.ajax.reload(); 
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(' update data error');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            }
        });
    }

	function edit_rkakl(id)
	{
		save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('rkakl/ajax_edit/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
				$('[name="realisasi_bulan_lalu"]').val(data.realisasi_bulan_lalu);
				$('[name="realisasi_bulan_ini"]').val(data.realisasi_bulan_ini);

				$('#created_date').datepicker('setDate', data.created_date);
				$('#created_date').datepicker('update');

                // $('[name="created_date"]').val(data.created_date);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
	}

	
</script>