<title>
	Kemendesa | BALILATFO
</title>
<div class="m-grid__item m-grid__item--fluid m-wrapper">		
	<div class="m-subheader ">
	<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					BALILATFO
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url();?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								BALILATFO
							</span>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>

	<div class="m-content" style="width:100%"> 
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-accent"></i>
		</div>
		<div class="m-alert__text">
		Badan Penelitian Dan Pengembangan, Pendidikan Dan Pelatihan, Dan Informasi Kementerian Desa, Pembangunan Daerah Tertinggal Dan Transmigrasi Republik Indonesia
		</div>
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						BALILATFO DATA
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin: Search Form -->
			<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="form-group m-form__group row align-items-center">
							<div class="col-md-4">
							</div>
						</div>
					</div>
					<div class="col-xl-4 order-1 order-xl-2 m--align-right">
						<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air" onclick="add_balilatfo()">
							<span>
								<i class="la la-plus"></i>
								<span>
									New Balilatfo Data
								</span>
							</span>
						</a>
						<div class="m-separator m-separator--dashed d-xl-none"></div>
					</div>
				</div>
			</div>
			<!--end: Search Form -->
<!--begin: Datatable -->
			<div class="m_datatable m-datatable 
			m-datatable--default m-datatable--error m-datatable--scroll m-datatable--loaded" 
			id="table2" style="overflow-x:auto;">
			<!-- <table class="m-datatable__table" 
				id="table"
				style="display: block; min-height: 300px; overflow-x: auto;">
				<thead class="m-datatable__head">
					<tr class="m-datatable__row" style="left: 0px;"><th data-field="RecordID" 
					class="m-datatable__cell--center m-datatable__cell m-datatable__cell--sort" rowspan="2">
					<span style="width: 40px;" >Action</span></th>
					<th data-field="nama_satker" class="m-datatable__cell m-datatable__cell--sort" rowspan="2"><span style="width: 150px;" >Satker</span></th>
					<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">PAGU</span></th>					
					<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2" ><span style="width: 170px;"></span></th>
					<th data-field="fisik" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 200px;">REALISASI</span></th>
					<th data-field="belanjapegawai_pagu" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">PAGU</span></th>
					<th data-field="belanjapegawai_realisasi" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 170px;">REALISASI</span></th>
					<th data-field="belanjabarang_pagu" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">PAGU</span></th>
					<th data-field="belanjabarang_realisasi" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">PAGU</span></th>
					<th data-field="belanjamodal_pagu" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">PAGU</span></th>
					<th data-field="belanjamodal_realisasi" class="m-datatable__cell m-datatable__cell--sort"><span style="width: 110px;">PAGU</span></th>
					</tr>
					<tr>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">AWAL</span></th>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">REVISI</span></th>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">Neto</span></th>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">%</span></th>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">REVISI4</span></th>
						<th data-field="pagu_awal" class="m-datatable__cell m-datatable__cell--sort" colspan="2"><span style="width: 170px;">REVISI5</span></th>
					</tr>
				</thead>
			</table> -->
			<table id="table" class="m-datatable__table" cellspacing="0" width="100%">
				<thead class="m-datatable__head">
				<tr class="m-datatable__row">
					<th rowspan="2">Action</th>
					<th rowspan="2">SATKER</th>
					<th colspan="2">PAGU</th>
					<th colspan="2">REALISASI</th>
					<th colspan="2">FISIK KEU</th>
					<th colspan="2">BELANJA PEGAWAI</th>
					<th colspan="2">BELANJA BARANG</th>
					<th colspan="2">BELANJA MODAL</th>
					<th>SISA</th>
				</tr>
				<tr>
					<th>AWAL</th>
					<th>REVISI</th>
					<th>Neto</th>
					<th>%</th>
					<th>Rp.</th>
					<th>%</th>
					<th>PAGU</th>
					<th>REALISASI</th>
					<th>PAGU</th>
					<th>REALISASI</th>
					<th>PAGU</th>
					<th>REALISASI</th>
					<th>PAGU</th>
				</tr>
				</thead>
				<tbody>
				</tbody>
			</table>

			</div>
			<!--end: Datatable -->
		</div>
	</div>
</div>

<!--Modal 1-->
        <!-- Bootstrap modal -->
        <div class="modal fade" id="modal_form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
					<div class="col-md-12">
					<div class="m-portlet m-portlet--tab">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
											<h3 class="m-portlet__head-text">
												Add Data Pagu, Realisasi
											</h3>
										</div>
									</div>
								</div>
								<!--begin::Form-->
								<form class="m-form m-form--fit m-form--label-align-right" id="form">
									<div class="m-portlet__body">
										<div class="form-group m-form__group">
											<label>
												Nama SATKER
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control m-input" 
												id="namaSatker" name="namaSatker"
												placeholder="Nama Satker">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Pagu Awal
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" class="form-control m-input"
												id="paguAwal"  name="paguAwal"
												placeholder="Pagu Awal" >
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Fisik Keu Rp.
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="fisikKeuRp" name="fisikKeuRp"
												class="form-control m-input" placeholder="Fisik Keu Rp.">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Pagu Belanja Pegawai
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="paguBelanjaPegawai" name="paguBelanjaPegawai"
												class="form-control m-input" placeholder="Pagu Belanja Pegawai">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Realisasi Belanja Pegawai
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="realisasiBelanjaPegawai" name="realisasiBelanjaPegawai"
												class="form-control m-input" placeholder="Realisasi Belanja Pegawai">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Pagu Belanja Barang
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="paguBelanjaBarang" name="paguBelanjaBarang"
												class="form-control m-input" placeholder="Pagu Belanja Barang">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Realisasi Belanja Barang
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" class="form-control m-input" 
												id="realisasiBelanjaBarang" name="realisasiBelanjaBarang"
												placeholder="Realisasi Belanja Barang">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="form-group m-form__group">
											<label>
												Pagu Belanja Modal
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="paguBelanjaModal" name="paguBelanjaModal"
												class="form-control m-input" placeholder="Pagu Belanja Modal">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<input type="hidden" value="" name="id"/>
										<div class="form-group m-form__group">
											<label>
												Realisasi Belanja Modal
											</label>
											<div class="m-input-icon m-input-icon--left">
												<input type="number" 
												id="realisasiBelanjaModal" name = "realisasiBelanjaModal"
												class="form-control m-input" placeholder="Realisasi Belanja Modal">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span>
														<i class="la la-circle"></i>
													</span>
												</span>
											</div>
										</div>
										<div class="m-form__actions">
											<button type="submit" class="btn btn-success" 
												onclick="save()" id="btnSave">
												Submit
											</button>
										</div>
									</div>
								</form>
								<!--end::Form-->
							</div>
                </div><!-- /.modal-content -->
					</div>
				
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End Bootstrap modal -->
        <!--End Modal1-->

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
		
<script type="text/javascript">
	var table;
	$(document).ready(function() {
	table = $('#table').DataTable({
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"order": [], //Initial no order.
		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": "<?php echo site_url('balilatfo/ajax_list')?>",
			"type": "POST"
		},
		//Set column definition initialisation properties.
		"columnDefs": [
			{
				"targets": [ 0 ], //first column / numbering column
				"orderable": true //set not orderable
			},
		],
        "dom": 'Blfrtip',
		"buttons": [
            {
                extend: 'excel',
				text: '<span><i class="la la-file-excel-o"></i><span>Excel Report</span></span>',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
					},
					columns: 'th:not(:first-child)'
                }
            }
        ],

	});
	});
	function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
    }
	function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('balilatfo/ajax_add')?>";
        } else {
            url = "<?php echo site_url('balilatfo/ajax_update')?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
					$('#modal_form').modal('hide');
                    reload_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            }
        });
    }
	function delete_balilatfo(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('balilatfo/ajax_delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
        }
	}
	
	function add_balilatfo()
    {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('New Balilatfo Information'); // Set Title to Bootstrap modal title
	}
	
	function edit_balilatfo(id)
	{
		save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('balilatfo/ajax_edit/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
				$('[name="namaSatker"]').val(data.nama_satker);
				$('[name="paguAwal"]').val(data.pagu_awal);
                $('[name="fisikKeuRp"]').val(data.fisikkeu_rp);
                $('[name="paguBelanjaPegawai"]').val(data.belanjapegawai_pagu);
                $('[name="realisasiBelanjaPegawai"]').val(data.belanjapegawai_realisasi);
				$('[name="paguBelanjaBarang"]').val(data.belanjabarang_pagu);				
				$('[name="realisasiBelanjaBarang"]').val(data.belanjabarang_realisasi);
				$('[name="paguBelanjaModal"]').val(data.belanjapegawai_realisasi);
				$('[name="realisasiBelanjaModal"]').val(data.belanjamodal_realisasi);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
	}

</script>