<script src="https://www.gstatic.com/charts/loader.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>application/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script type="text/javascript">

// google.charts.load('current', {packages: ['corechart', 'bar']});
// 	google.charts.setOnLoadCallback(drawChart);
// 	function drawChart() {
//         var data = google.visualization.arrayToDataTable([
//             ['Satker', 'Jumlah'],
//             <?php
//             foreach ($chart_data as $data) {
//                 echo '['. "'". $data->nama_satker . "'". ',' . $data->pagu_awal . '],';
//             }
//             ?>
//         ]);
//         var options = {
//             chart: {
//                 title: 'Statistics Alokasi Anggaran Per Satuan Kerja',
//                 chartArea: {width: '50%'},
//                 isStacked: true,
//             }
//         };
//         var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
//         chart.draw(data, options);
//     }
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {
      var data = google.visualization.arrayToDataTable([
		['Nama Satker', 'Pagu Awal', 'Realisasi Neto'],
		<?php
			foreach ($chart_data as $data) {
				echo '['. "'". $data->nama_satker . "'". ',' . $data->pagu_awal . ',' . $data->sisa .'],';
			}
		?>        
      ]);

      var options = {
        title: 'Statistics Alokasi Anggaran Per Satuan Kerja',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Nominal',
          minValue: 0
        },
        vAxis: {
          title: 'Satuan Kerja'
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('columnchart_material'));
      chart.draw(data, options);
    }
</script>
<title>
			Kemendesa | Dashboard
		</title>
					
					<div class="m-content" style="width:100%"> 
						<div class="row">							
							<div class="col-xl-12">
								<!--begin:: Widgets/Best Sellers-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
												BALILATFO STATISTICS
												</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
										<!--begin::Content-->
										<div class="tab-content">
											<div id="columnchart_material" style="width:100%; height:900px"></div>
										</div>
										<!--end::Content-->
									</div>
								</div>
								<!--end:: Widgets/Best Sellers-->
							</div>
						</div>
						<!--End::Section-->
					</div>

