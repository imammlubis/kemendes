<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chart_model extends CI_Model{

    var $table = 'balilatfo';
    var $column_order = array(null,'id', 'nama_satker','pagu_awal','fisikkeu_rp'); //set column field database for datatable orderable
    var $column_search = array('nama_satker'); //set column field database for datatable searchable
    var $order = array('id' => 'asc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function get_chart_data() {
        
        $this->db->select('nama_satker, 
        pagu_awal, (belanjapegawai_realisasi + belanjabarang_realisasi + belanjamodal_realisasi) as sisa');
        $this->db->from('balilatfo');
        $query = $this->db->get();
        $results['chart_data'] = $query->result();

        return $results;
        
    }
}