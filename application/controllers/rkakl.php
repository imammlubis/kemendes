<?php defined('BASEPATH') OR exit('No direct script access allowed');

class rkakl extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        //$this->load->library('session');
        $this->load->model('auth/user_model');
        $this->load->helper('auth/user_helper');
        $this->load->model('rkakl_model');
        $this->load->library('excel');
    }

    function index(){
        check_user_sess();
        if($this->session->userdata('logged_in'))
        {
            $data ['main_content'] = 'rkakl';
            $this->load->view('layout/MainLayout', $data);
        }
        else{
            redirect('account/user');
        }
    }

    public function ajax_list()
    {
        $list = $this->rkakl_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $tabel) {
            $no++;
            $row = array();
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_rkakl('."'".$tabel->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
            $row[] = $tabel->kode;
            $row[] = $tabel->uraian;
            $row[] = number_format($tabel->vol);  
            $row[] = $tabel->sat;
            $row[] = number_format($tabel->hargasat);  
            $row[] = number_format($tabel->jumlah);  
            $row[] = $tabel->sdana;
            $row[] = number_format($tabel->realisasi_bulan_lalu);  
            $row[] = number_format($tabel->realisasi_bulan_ini);  
            $row[] = number_format($tabel->realisasi_rp); 
            $row[] = number_format($tabel->realisasi_percent, 2); 
            $row[] = number_format($tabel->sisa_anggaran); 
            $row[] = date("d/m/Y", strtotime($tabel->created_date)) == "01/01/1970" ? "" : date("d/m/Y", strtotime($tabel->created_date));
            // $row[] = $tabel->upload_date;
            // $row[] = $tabel->header_kode;          
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->rkakl_model->count_all(),
            "recordsFiltered" => $this->rkakl_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_update()
    {
        $this->_validate();
       // $tempjumlah = $this->rkakl_model->get_jumlah_by_id($this->input->post('id'));
        $data = array(
            'realisasi_bulan_lalu' => $this->input->post('realisasi_bulan_lalu'),
            'realisasi_bulan_ini' => $this->input->post('realisasi_bulan_ini'),
            'realisasi_rp' =>$this->input->post('realisasi_bulan_lalu')+$this->input->post('realisasi_bulan_ini'),
            //'realisasi_percent' => (($this->input->post('realisasi_bulan_lalu')+$this->input->post('realisasi_bulan_ini')) / $tempjumlah) *100,
            'realisasi_percent' => round(((($this->input->post('realisasi_bulan_lalu')+$this->input->post('realisasi_bulan_ini')) / $data1->jumlah) *100), 2),
            'created_date' => $this->input->post('created_date'),
        );
        $this->rkakl_model->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => $data('realisasi_percent')));
    }

    public function ajax_edit($id)
    {
        $data = $this->rkakl_model->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('realisasi_bulan_lalu') == '')
        {
            $data['inputerror'][] = 'realisasi_bulan_lalu';
            $data['error_string'][] = 'S/D BULAN LALU (Rp) is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('realisasi_bulan_ini') == '')
        {
            $data['inputerror'][] = 'realisasi_bulan_ini';
            $data['error_string'][] = 'BULAN INI is required';
            $data['status'] = FALSE;
        }


        // if($this->input->post('fisikKeuRp') == '')
        // {
        //     $data['inputerror'][] = 'fisikKeuRp';
        //     $data['error_string'][] = 'fisikKeuRp is required';
        //     $data['status'] = FALSE;
        // }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

    function import()
	{
		if(isset($_FILES["file"]["name"]))
		{
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
                    $header_kode = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $kode = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    if($kode == null || $kode == '')
                    {
                        $header_kode = $tempKode;
                    }
                    else
                    {
                        $tempKode = $kode;
                    }
					$uraian = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$vol = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$sat = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$hargasat = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$jumlah = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$sdana = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$realisasi_bulan_lalu = 0;
					$realisasi_bulan_ini = 0;
					$realisasi_rp = 0;
					$realisasi_percent = 0;
                    $sisa_anggaran = 0;
                    //$upload_date = date("Y-m-d H:i:s");

					    $data[] = array(
					    	'kode'		        =>	$kode,
					    	'uraian'			=>	$uraian,
					    	'vol'				=>	intval($vol),
					    	'sat'		        =>	$sat,
					    	'hargasat'		    =>	$hargasat,
					    	'jumlah'		   	=>	$jumlah,
					    	'sdana'		    	=>	$sdana,
					    	'realisasi_bulan_lalu'		    	=>	$realisasi_bulan_lalu,
					    	'realisasi_bulan_ini'		    	=>	$realisasi_bulan_ini,
					    	'realisasi_rp'		    	=>	$realisasi_rp,
					    	'realisasi_percent'		    =>	$realisasi_percent,
                            'sisa_anggaran'		    	=>	$sisa_anggaran,
                            'header_kode'               => $header_kode
                            //'upload_date'               => $upload_date
                        );
				}
			}
			$this->rkakl_model->bulkSave($data);
            echo 'Data Imported successfully';
            
		}	
    }
    
    function export_excel()
	{
		$object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        
        $styleCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $styleBold = array(
            'font' => array(
                'bold' => true,
            )
        );
        $styleBorder = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "LAPORAN REALISASI");
        $object->getActiveSheet()->mergeCells('B1:G1');
        $object->getActiveSheet()->getStyle("B1:G1")->applyFromArray($styleCenter);
        $object->getActiveSheet()->getStyle("B1:G1")->applyFromArray($styleBold);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, 2, "BALAI LATIHAN MASYARAKAT DENPASAR");
        $object->getActiveSheet()->mergeCells('B2:G2');
        $object->getActiveSheet()->getStyle("B2:G2")->applyFromArray($styleCenter);
        $object->getActiveSheet()->getStyle("B2:G2")->applyFromArray($styleBold);
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, 3, "SEPTEMBER 2018");
        $object->getActiveSheet()->mergeCells('B3:G3');
        $object->getActiveSheet()->getStyle("B3:G3")->applyFromArray($styleCenter);
        $object->getActiveSheet()->getStyle("B3:G3")->applyFromArray($styleBold);

		$table_columns = array("Kode", "PROGRAM/KEGIATAN/OUTPUT/SUBOUTPUT/ KOMPONEN/SUBKOMP/AKUN/DETIL", "REALISASI KEUANGAN");

		$column = 0;

		foreach($table_columns as $field)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 6, $field);
			$column++;
        }
        
        $object->getActiveSheet()->mergeCells('A6:A9'); //Kode
        $object->getActiveSheet()->getStyle("A6:A9")->applyFromArray($styleCenter);
        $object->getActiveSheet()->mergeCells('B6:B9'); //Uraian
        $object->getActiveSheet()->getStyle("B6:B9")->applyFromArray($styleCenter);
        $object->getActiveSheet()->getStyle("B6:B9")->applyFromArray($styleBold);
        $object->getActiveSheet()->mergeCells('C6:G6'); //Realisasi Keuangan
        $object->getActiveSheet()->getStyle("C6:G6")->applyFromArray($styleCenter);
        $object->getActiveSheet()->setCellValueByColumnAndRow(7, 6, "SISA ANGGARAN");
        $object->getActiveSheet()->mergeCells('H6:H9'); //Sisa Anggaran
        $object->getActiveSheet()->getStyle("H6:H9")->applyFromArray($styleCenter);

        $object->getActiveSheet()->setCellValueByColumnAndRow(2, 7, "PAGU REVISI");
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, 9, "(Rp)");

        $object->getActiveSheet()->setCellValueByColumnAndRow(3, 7, "S/D");
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, 8, "BLN LALU");
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, 9, "(Rp)");

        $object->getActiveSheet()->setCellValueByColumnAndRow(4, 7, "BULAN INI");
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, 9, "(Rp)");

        $object->getActiveSheet()->setCellValueByColumnAndRow(5, 7, "S/D BULAN INI");
        $object->getActiveSheet()->mergeCells('F7:G7'); //S/D BULAN INI
        $object->getActiveSheet()->getStyle("F7:G7")->applyFromArray($styleCenter);
        $object->getActiveSheet()->setCellValueByColumnAndRow(5, 9, "(Rp)");
        $object->getActiveSheet()->setCellValueByColumnAndRow(6, 9, "(%)");

        $object->getActiveSheet()->duplicateStyle($object->getActiveSheet()->getStyle('B6'), 'C6:H9');

		$anggaran_data = $this->rkakl_model->fetch_data();

		$excel_row = 10;

		foreach($anggaran_data as $row)
		{
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->kode);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->uraian);
            
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->jumlah);
            $object->getActiveSheet()->getStyle('C:C')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->getStyle('C:C')->getNumberFormat()->setFormatCode("#,##0,00");
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->realisasi_bulan_lalu);
            $object->getActiveSheet()->getStyle('D:D')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->getStyle('D:D')->getNumberFormat()->setFormatCode("#,##0,00");
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->realisasi_bulan_ini);
            $object->getActiveSheet()->getStyle('E:E')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->getStyle('E:E')->getNumberFormat()->setFormatCode("#,##0,00");
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->realisasi_rp);
            $object->getActiveSheet()->getStyle('F:F')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->getStyle('F:F')->getNumberFormat()->setFormatCode("#,##0,00");
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->realisasi_percent);
            $object->getActiveSheet()->getStyle('G:G')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->sisa_anggaran);
            $object->getActiveSheet()->getStyle('H:H')->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            $object->getActiveSheet()->getStyle('H:H')->getNumberFormat()->setFormatCode("#,##0,00");
			$excel_row++;
        }
        
        //set width
        $object->getActiveSheet()->getColumnDimension('B')->setWidth(120); 
        $object->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
        $object->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
        $object->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
        $object->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
        $object->getActiveSheet()->getColumnDimension('G')->setWidth(10); 
        $object->getActiveSheet()->getColumnDimension('H')->setWidth(20); 

        $object->getActiveSheet()->getStyle("A6:H".$excel_row)->applyFromArray($styleBorder);

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="ALUR APLIKASI SIMPAN ONLINE BALILATFO.xls"');
		$object_writer->save('php://output');
	}

}
?>