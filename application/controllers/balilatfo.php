<?php defined('BASEPATH') OR exit('No direct script access allowed');

class balilatfo extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        //$this->load->library('session');
        $this->load->model('auth/user_model');
        $this->load->helper('auth/user_helper');
        $this->load->model('balilatfo_model');
    }

    function index(){
        check_user_sess();
        if($this->session->userdata('logged_in'))
        {
            $data ['main_content'] = 'balilatfo';
            $this->load->view('layout/MainLayout', $data);
        }
        else{
            redirect('account/user');
        }
    }

    public function ajax_list()
    {
        $list = $this->balilatfo_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $tabel) {
            $pagu_revisi = $tabel->belanjapegawai_pagu + $tabel->belanjabarang_pagu + $tabel->belanjamodal_pagu;
            $realisasi_neto = $tabel->belanjapegawai_realisasi + $tabel->belanjabarang_realisasi + $tabel->belanjamodal_realisasi;
            $sisa = $pagu_revisi - $realisasi_neto;
            $no++;
            $row = array();
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_balilatfo('."'".$tabel->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_balilatfo('."'".$tabel->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $row[] = $tabel->nama_satker;
            $row[] = number_format($tabel->pagu_awal);
            $row[] = number_format($pagu_revisi);
            $row[] = number_format($realisasi_neto);
            $row[] = number_format($realisasi_neto/$pagu_revisi*100, 2);
            $row[] = number_format($tabel->fisikkeu_rp);
            $row[] = number_format($tabel->fisikkeu_rp/$pagu_revisi*100, 2);
            $row[] = number_format($tabel->belanjapegawai_pagu);
            $row[] = number_format($tabel->belanjapegawai_realisasi);
            $row[] = number_format($tabel->belanjabarang_pagu);
            $row[] = number_format($tabel->belanjabarang_realisasi);
            $row[] = number_format($tabel->belanjamodal_pagu);
            $row[] = number_format($tabel->belanjamodal_realisasi);    
            $row[] = number_format($sisa);    
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->balilatfo_model->count_all(),
            "recordsFiltered" => $this->balilatfo_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    public function ajax_add()
    {
        $this->_validate();
        $data = array(
            'nama_satker' => $this->input->post('namaSatker'),
            'pagu_awal' => $this->input->post('paguAwal'),
            'fisikkeu_rp' => $this->input->post('fisikKeuRp'),
            'belanjapegawai_pagu' => $this->input->post('paguBelanjaPegawai'),
            'belanjapegawai_realisasi' => $this->input->post('realisasiBelanjaPegawai'),
            'belanjabarang_pagu' => $this->input->post('paguBelanjaBarang'),
            'belanjabarang_realisasi' => $this->input->post('realisasiBelanjaBarang'),
            'belanjamodal_pagu' => $this->input->post('paguBelanjaModal'),
            'belanjamodal_realisasi' => $this->input->post('realisasiBelanjaModal')
            // 'balilatfo_date' => $this->input->post('balilatfo_date')
        );
        $insert = $this->balilatfo_model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'nama_satker' => $this->input->post('namaSatker'),
            'pagu_awal' => $this->input->post('paguAwal'),
            'fisikkeu_rp' => $this->input->post('fisikKeuRp'),
            'belanjapegawai_pagu' => $this->input->post('paguBelanjaPegawai'),
            'belanjapegawai_realisasi' => $this->input->post('realisasiBelanjaPegawai'),
            'belanjabarang_pagu' => $this->input->post('paguBelanjaBarang'),
            'belanjabarang_realisasi' => $this->input->post('realisasiBelanjaBarang'),
            'belanjamodal_pagu' => $this->input->post('paguBelanjaModal'),
            'belanjamodal_realisasi' => $this->input->post('realisasiBelanjaModal')
        );
        $this->balilatfo_model->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->balilatfo_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }


    public function ajax_edit($id)
    {
        $data = $this->balilatfo_model->get_by_id($id);
        //$data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }
    
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('namaSatker') == '')
        {
            $data['inputerror'][] = 'namaSatker';
            $data['error_string'][] = 'namaSatker is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('paguAwal') == '')
        {
            $data['inputerror'][] = 'paguAwal';
            $data['error_string'][] = 'paguAwal is required';
            $data['status'] = FALSE;
        }


        // if($this->input->post('fisikKeuRp') == '')
        // {
        //     $data['inputerror'][] = 'fisikKeuRp';
        //     $data['error_string'][] = 'fisikKeuRp is required';
        //     $data['status'] = FALSE;
        // }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}