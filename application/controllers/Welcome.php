<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Created By Imammlubis
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        //$this->load->library('session');
        $this->load->model('auth/user_model');
        $this->load->helper('auth/user_helper');
        $this->load->model('balilatfo_model');
        $this->load->model('chart_model');
    }
	public function index()
	{
		check_user_sess();
        if($this->session->userdata('logged_in'))
        {
            $results = $this->chart_model->get_chart_data();
            $data['chart_data'] = $results['chart_data'];

            $data ['main_content'] = 'dashboard';
            $this->load->view('layout/MainLayout', $data);
        }
        else{
            redirect('account/user');
        }
    }
    
    public function getChartData(){
        $data ['main_content'] = 'dashboard';
            $this->load->view('layout/MainLayout', $data);
        $list = $this->balilatfo_model->get_chart();
        return $list;
    }

}
